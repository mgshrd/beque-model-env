type BequeResult ok err = Prelude.Either err ok

coqstring2string :: String -> Prelude.String
coqstring2string s = case s of { EmptyString -> ""; String0 c sx -> c:(coqstring2string sx) }
string2coqstring :: Prelude.String -> String
string2coqstring s = Prelude.foldr (\e accu -> if Data.Char.ord e Prelude.> 255 then Prelude.error ("invalid coq character " Prelude.++ (Prelude.show Prelude.$ Data.Char.ord e)) else String0 e accu) EmptyString s

instance Prelude.Show String where show = coqstring2string
instance Prelude.Read String where readsPrec = \_ s -> [(string2coqstring s, "")]
