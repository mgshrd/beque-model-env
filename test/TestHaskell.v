Require Import String.

Require Beque.Util.common_extract.
Require Beque.Util.DelayExtractionDefinitionToken.

Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import Beque.IO.intf.IOAux.

Require Import EnvImpl.

Extraction Language Haskell.

Module comext.
Include common_extract.commonExtractHaskell DelayExtractionDefinitionToken.token.
End comext.

Module ioext.
Include AxiomaticIOInAUnitWorld.extractHaskell DelayExtractionDefinitionToken.token.
End ioext.

Module ior := IOAux UnitWorld AxiomaticIOInAUnitWorld.
Import ior.

Module env.
Include EnvImpl AxiomaticIOInAUnitWorld.UnitWorld AxiomaticIOInAUnitWorld.
Include env.extractHaskell DelayExtractionDefinitionToken.token.
End env.

Module Dump.

Axiom dump : string -> AxiomaticIOInAUnitWorld.IO (fun _ => True) unit (fun _ _ _ => True).

Module extractHaskell (token : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant dump => "Prelude.putStrLn Prelude.. Prelude.show".
End extractHaskell.

Module extractOcaml (token : DelayExtractionDefinitionToken.DelayExtractionDefinitionToken).
Extract Constant dump => "fun s -> fun () -> print_endline (coqstring2string s)".
End extractOcaml.

End Dump.

Module DumpE.
Include Dump.extractHaskell DelayExtractionDefinitionToken.token.
End DumpE.

Program Definition main :=
  h <== env.get_env_var "testvar";;
    /| Dump.dump (match h with result.ok v => v | result.err e => append "missing env var: " e end).

Extraction "test" main.
