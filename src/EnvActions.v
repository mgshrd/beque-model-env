Require Import String.

Require Import Beque.Util.result.

Module EnvActions
.

Inductive env_action :=
| env_action__get_env_var : string -> env_action.

Definition eq_env_action_dec :
  forall a1 a2 : env_action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  set (H_eq_string_dec := string_dec).
  decide equality.
Defined.

Definition env_action_res
           (a : env_action) :
  Set :=
  match a with
  | env_action__get_env_var name => result string string
  end.

Definition eq_env_action_res_dec :
  forall (a : env_action) (res1 res2 : env_action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  set (H_eq_string_dec := string_dec).
  destruct a; simpl in *; decide equality.
Defined.

End EnvActions.
