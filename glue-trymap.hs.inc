trymap :: Prelude.IO a -> Prelude.IO (Prelude.Either String a)
trymap = ((Prelude.fmap (Data.Bifunctor.first Prelude.$ string2coqstring Prelude.. Prelude.show)) :: (Prelude.Functor f, Data.Bifunctor.Bifunctor g) => f (g Control.Exception.SomeException b) -> f (g String b)) Prelude.. Control.Exception.try

trymap_stderr :: Prelude.IO a -> Prelude.IO (Prelude.Either String a)
trymap_stderr a =
  do
    e <- trymap a
    case e of
      Prelude.Left msg -> System.IO.hPutStrLn System.IO.stderr Prelude.$ Prelude.show msg
      Prelude.Right _ -> Prelude.return ()
    Prelude.return e
